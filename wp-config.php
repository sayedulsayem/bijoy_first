<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'bijoy_first');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '.7L3~0}|ZVnwb<mI%d4^pG&^+ZNHUN&Mv/g2NoeExv*#)/9Aq&,R75#:?7B@#7V*');
define('SECURE_AUTH_KEY',  'jjxJBFvQJ>6w*[Uqx3dX?^*gs|*b5lxbPNv{4(|]MsCYQXujD([RZ|$t`yy|`Glx');
define('LOGGED_IN_KEY',    'VOa=1.zU]/E.H=sW?8>lXKwP:I+pqUsCd8c!qgZ.+t_z6$Tw-jsemZa7}7mURd`j');
define('NONCE_KEY',        'G-zNV-GgAokyU8D9PKq8IB.V(#&O_E+1Fp46eV2,F80|[%|:G2ok;g5sYZUSJ&l?');
define('AUTH_SALT',        'S%%bP8CPDlMNC!I6A<Ip=14%j(rgZ*X5WGGXQk>M$~_cnwQDnn#yn#b`M.,*)(E^');
define('SECURE_AUTH_SALT', 'fvWZM7BZnqp|Lb%%0JI&>aKq}$r1X)8YU[){|*.5TVt8jr-#Q|]C ubjme2riMr&');
define('LOGGED_IN_SALT',   'F79h}sSRqyIl,L~G1pzP&gvzX8]P`ySSDuR8r=siK!A):N|({$|?;Gb,Q[se+~`4');
define('NONCE_SALT',       '4a{nV|+/U#_7[i`y1XS[8E+~nW>VIS%j-#bq`S4XH2iW#[U6FL1f*SY13A@ZPcoT');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
