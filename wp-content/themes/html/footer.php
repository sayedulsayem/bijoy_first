<!-- Footer Section -->
<footer id="footer">
    <div class="footer-container">
        <div class="container">
            <div class="footer-widget">

                <div class="col-md-4">
                    <div class="widget">
                        <div class="widget-title">
                            <h3>Contact us</h3>
                        </div><!-- /.widget-title -->
                        <div class="address">
                            <p>
                                <span class="icon"><i class="fa fa-map-marker"></i></span>
                                <span class="txt"> Dhanmondi -5 , Dhaka 1205 Dhaka, Bangladesh</span>
                            </p>
                            <p>
                                <span class="icon"><i class="fa fa-phone"></i></span>
                                <span class="txt"> +01515219181 </span>
                            </p>
                            <p>
                                <span class="icon"><i class="fa fa-envelope"></i></span>
                                <span class="txt"> sayedulsayem@gmail.com</span>
                            </p>
                        </div><!-- /.address -->
                    </div><!-- /.widget -->
                </div><!-- /.col-md-4 -->

                <div class="col-md-4">
                    <div class="widget quick-link">
                        <h3>Contact us</h3>
                        <a href="#">Home</a>
                        <a href="#">About</a>
                        <a href="#">Blog</a>
                        <a href="#">Portfolio</a>
                        <a href="#">Content</a>
                    </div><!-- /.widget -->

                </div><!-- /.col-md-4 -->

                <div class="col-md-4">
                    <div class="widget latest-tweets">
                        <h3 class="widget-title">Latest Tweets</h3>
                        <a href="#">Lipsum dolor sit amet, consectetur adipiscing euismod velit lacinia. Vestibulum tristique...</a>
                        <p>Crative Design 6 hours ago , Shuily Yeasin </p>
                    </div><!-- /.widget -->

                    <div class="widget latest-tweets">
                        <a href="#">Lipsum dolor sit amet, consectetur adipiscing euismod velit lacinia. Vestibulum tristique...</a>
                        <p>Crative Design 6 hours ago , Shuily Yeasin </p>
                    </div><!-- /.widget -->

                    <div class="widget latest-tweets">
                        <a href="#">Lipsum dolor sit amet, consectetur adipiscing euismod velit lacinia. Vestibulum tristique...</a>
                        <p>Crative Design 6 hours ago , Shuily Yeasin </p>
                    </div><!-- /.widget -->
                </div><!-- /.col-md-4 -->

            </div><!-- /.footer-widget -->
        </div><!-- /.container -->
    </div><!-- /.footer-container -->
</footer><!-- /#footer -->
<!-- Footer Section End -->


<!-- For Js Library -->
<script src="assets/js/jquery-2.1.3.min.js"></script>

<!-- For Js Plugins -->
<script src="assets/js/plugins.js"></script>

<!-- Include Functions.js -->
<script src="assets/js/functions.js"></script>
<?php wp_footer(); ?>
</body>
</html>