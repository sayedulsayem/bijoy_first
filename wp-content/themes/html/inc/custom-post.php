<?php

/* function slider_post(){

	$args = array(

		'label'=>'Sliders',
		'public'=>true,
		'menu_position'=>10,
		'supports'=>array('title','editor','thumbnail','excerpt'),
	);
	register_post_type('slider',$args);
}
add_action('init','slider_post'); */



function slider_post() {
	$labels = array(
		'name'               => _x( 'Sliders', 'post type general name', 'your-plugin-textdomain' ),
		'singular_name'      => _x( 'Slider', 'post type singular name', 'your-plugin-textdomain' ),
		'menu_name'          => _x( 'Sliders', 'admin menu', 'your-plugin-textdomain' ),
		'name_admin_bar'     => _x( 'Slider', 'add new on admin bar', 'your-plugin-textdomain' ),
		'add_new'            => _x( 'Add New', 'Slider', 'your-plugin-textdomain' ),
		'add_new_item'       => __( 'Add New Slider', 'your-plugin-textdomain' ),
		'new_item'           => __( 'New Slider', 'your-plugin-textdomain' ),
		'edit_item'          => __( 'Edit Slider', 'your-plugin-textdomain' ),
		'view_item'          => __( 'View Slider', 'your-plugin-textdomain' ),
		'all_items'          => __( 'All Sliders', 'your-plugin-textdomain' ),
		'search_items'       => __( 'Search Sliders', 'your-plugin-textdomain' ),
		'parent_item_colon'  => __( 'Parent Sliders:', 'your-plugin-textdomain' ),
		'not_found'          => __( 'No Sliders found.', 'your-plugin-textdomain' ),
		'not_found_in_trash' => __( 'No Sliders found in Trash.', 'your-plugin-textdomain' )
	);

	$args = array(
		'labels'             => $labels,
		'description'        => __( 'Description.', 'your-plugin-textdomain' ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'Slider' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )
	);

	register_post_type( 'slider', $args );
}
add_action( 'init', 'slider_post' );


//Team Custom post type

add_action( 'init', 'team_post_type' );

function team_post_type() {
	$labels = array(
		'name'               => _x( 'Teams', 'post type general name', 'your-plugin-textdomain' ),
		'singular_name'      => _x( 'Team', 'post type singular name', 'your-plugin-textdomain' ),
		'menu_name'          => _x( 'Teams', 'admin menu', 'your-plugin-textdomain' ),
		'name_admin_bar'     => _x( 'Team', 'add new on admin bar', 'your-plugin-textdomain' ),
		'add_new'            => _x( 'Add New', 'Team', 'your-plugin-textdomain' ),
		'add_new_item'       => __( 'Add New Team', 'your-plugin-textdomain' ),
		'new_item'           => __( 'New Team', 'your-plugin-textdomain' ),
		'edit_item'          => __( 'Edit Team', 'your-plugin-textdomain' ),
		'view_item'          => __( 'View Team', 'your-plugin-textdomain' ),
		'all_items'          => __( 'All Teams', 'your-plugin-textdomain' ),
		'search_items'       => __( 'Search Teams', 'your-plugin-textdomain' ),
		'parent_item_colon'  => __( 'Parent Teams:', 'your-plugin-textdomain' ),
		'not_found'          => __( 'No Teams found.', 'your-plugin-textdomain' ),
		'not_found_in_trash' => __( 'No Teams found in Trash.', 'your-plugin-textdomain' )
	);

	$args = array(
		'labels'             => $labels,
		'description'        => __( 'Description.', 'your-plugin-textdomain' ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'Team' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )
	);

	register_post_type( 'team', $args );
}

//Register services custom post type


function service_post_type() {
	$labels = array(
		'name'               => _x( 'Services', 'post type general name', 'your-plugin-textdomain' ),
		'singular_name'      => _x( 'Service', 'post type singular name', 'your-plugin-textdomain' ),
		'menu_name'          => _x( 'Services', 'admin menu', 'your-plugin-textdomain' ),
		'name_admin_bar'     => _x( 'Service', 'add new on admin bar', 'your-plugin-textdomain' ),
		'add_new'            => _x( 'Add New', 'Service', 'your-plugin-textdomain' ),
		'add_new_item'       => __( 'Add New Service', 'your-plugin-textdomain' ),
		'new_item'           => __( 'New Service', 'your-plugin-textdomain' ),
		'edit_item'          => __( 'Edit Service', 'your-plugin-textdomain' ),
		'view_item'          => __( 'View Service', 'your-plugin-textdomain' ),
		'all_items'          => __( 'All Services', 'your-plugin-textdomain' ),
		'search_items'       => __( 'Search Services', 'your-plugin-textdomain' ),
		'parent_item_colon'  => __( 'Parent Services:', 'your-plugin-textdomain' ),
		'not_found'          => __( 'No Services found.', 'your-plugin-textdomain' ),
		'not_found_in_trash' => __( 'No Services found in Trash.', 'your-plugin-textdomain' )
	);

	$args = array(
		'labels'             => $labels,
		'description'        => __( 'Description.', 'your-plugin-textdomain' ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'Service' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )
	);

	register_post_type( 'service', $args );
}
add_action( 'init', 'service_post_type' );

//Register service metabox

function service_meta_box(){

	add_meta_box(

		'service_icon',//id
		'Service Metabox',//title
		'service_icon_callback',//callback
		'service',//screen
		'normal',//context
		'high'//priority
	);

	//$screen = array('post','page','team','service');
}
add_action('add_meta_boxes','service_meta_box');

function service_icon_callback($post){

	$s_icon = get_post_meta($post->ID, 's_icon', true);
	wp_nonce_field('save_service_meta','service_nonce');
	?>
	<p>
		<label>Icon</label>
		<input type="text" name="s_icon" value="<?php echo $s_icon;?>"/>
	</p>
	<?php
}

function save_service_meta($post_id){

	// Check if our nonce is set.
	if(! isset($_POST['service_nonce'])){

		return;
	}

	// Verify that the nonce is valid.

	if(! wp_verify_nonce($_POST['service_nonce'],'save_service_meta')){

		return;
	}

	// Make sure that it(input) is set.

	if(! isset($_POST['s_icon'])){

		return;
	}

	// Sanitize user input.

	$my_s_icon = sanitize_text_field($_POST['s_icon']);

	// Update the meta field in the database

	update_post_meta($post_id,'s_icon',$my_s_icon);

}
add_action('save_post','save_service_meta');


/**
 * Register a Price post type.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 */
function pricing_custom_post() {
    $labels = array(
        'name'               => _x( 'Prices', 'post type general name', 'your-plugin-textdomain' ),
        'singular_name'      => _x( 'Price', 'post type singular name', 'your-plugin-textdomain' ),
        'menu_name'          => _x( 'Prices', 'admin menu', 'your-plugin-textdomain' ),
        'name_admin_bar'     => _x( 'Price', 'add new on admin bar', 'your-plugin-textdomain' ),
        'add_new'            => _x( 'Add New', 'Price', 'your-plugin-textdomain' ),
        'add_new_item'       => __( 'Add New Price', 'your-plugin-textdomain' ),
        'new_item'           => __( 'New Price', 'your-plugin-textdomain' ),
        'edit_item'          => __( 'Edit Price', 'your-plugin-textdomain' ),
        'view_item'          => __( 'View Price', 'your-plugin-textdomain' ),
        'all_items'          => __( 'All Prices', 'your-plugin-textdomain' ),
        'search_items'       => __( 'Search Prices', 'your-plugin-textdomain' ),
        'parent_item_colon'  => __( 'Parent Prices:', 'your-plugin-textdomain' ),
        'not_found'          => __( 'No Prices found.', 'your-plugin-textdomain' ),
        'not_found_in_trash' => __( 'No Prices found in Trash.', 'your-plugin-textdomain' )
    );

    $args = array(
        'labels'             => $labels,
        'description'        => __( 'Description.', 'your-plugin-textdomain' ),
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'Price' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )
    );

    register_post_type( 'pricing', $args );
}

add_action( 'init', 'pricing_custom_post' );

function pricing_metbox(){
    add_meta_box('pricing_meta','Pricing metbox','pricing_meta_callback','pricing','normal','high');
}

add_action('add_meta_boxes','pricing_metbox');

function pricing_meta_callback($post){

    $g_pricing_name = get_post_meta($post->ID,'pricing_name',true);
    $g_currency = get_post_meta($post->ID,'currency',true);
    $g_price = get_post_meta($post->ID,'price',true);

    wp_nonce_field('pricing_metabox','pricing_meta_nonce');

    ?>
    <p>
        <level>Package name : </level>
        <input type="text" name="pricing_name" value="<?php echo $g_pricing_name; ?>">
    </p>
    <p>
        <level>Currency : </level>
        <input type="text" name="currency" value="<?php echo $g_currency; ?>">
    </p>
    <p>
        <level>price : </level>
        <input type="text" name="price" value="<?php echo $g_price; ?>">
    </p>

<?php
}
function save_pricing_metadata($post_id){
    //checking nonce
    if(!isset($_POST['pricing_meta_nonce'])){
        return;
    }
    //varity nonce
    if (! wp_verify_nonce($_POST['pricing_meta_nonce'],'pricing_metabox')){
        return;
    }
    if(!isset($_POST['pricing_name'])){
        return;
    }
    if(!isset($_POST['currency'])){
        return;
    }
    if(!isset($_POST['price'])){
        return;
    }

    $s_pricing_name=sanitize_text_field($_POST['pricing_name']);
    $s_currency=sanitize_text_field($_POST['currency']);
    $s_price=sanitize_text_field($_POST['price']);

    update_post_meta($post_id,'pricing_name',$s_pricing_name);
    update_post_meta($post_id,'currency',$s_currency);
    update_post_meta($post_id,'price',$s_price);

}
add_action('save_post','save_pricing_metadata');

/**
 * Register a Portfolio post type.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 */
function custom_portfolio() {
    $labels = array(
        'name'               => _x( 'Portfolios', 'post type general name', 'your-plugin-textdomain' ),
        'singular_name'      => _x( 'Portfolio', 'post type singular name', 'your-plugin-textdomain' ),
        'menu_name'          => _x( 'Portfolios', 'admin menu', 'your-plugin-textdomain' ),
        'name_admin_bar'     => _x( 'Portfolio', 'add new on admin bar', 'your-plugin-textdomain' ),
        'add_new'            => _x( 'Add New', 'Portfolio', 'your-plugin-textdomain' ),
        'add_new_item'       => __( 'Add New Portfolio', 'your-plugin-textdomain' ),
        'new_item'           => __( 'New Portfolio', 'your-plugin-textdomain' ),
        'edit_item'          => __( 'Edit Portfolio', 'your-plugin-textdomain' ),
        'view_item'          => __( 'View Portfolio', 'your-plugin-textdomain' ),
        'all_items'          => __( 'All Portfolios', 'your-plugin-textdomain' ),
        'search_items'       => __( 'Search Portfolios', 'your-plugin-textdomain' ),
        'parent_item_colon'  => __( 'Parent Portfolios:', 'your-plugin-textdomain' ),
        'not_found'          => __( 'No Portfolios found.', 'your-plugin-textdomain' ),
        'not_found_in_trash' => __( 'No Portfolios found in Trash.', 'your-plugin-textdomain' )
    );

    $args = array(
        'labels'             => $labels,
        'description'        => __( 'Description.', 'your-plugin-textdomain' ),
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'Portfolio' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )
    );

    register_post_type( 'portfolio', $args );
}
add_action( 'init', 'custom_portfolio' );


//Register Portfolio Taxonomy

/* function portfolio_taxonomy(){

	register_taxonomy(

		'p_cat',
		'portfolio',
		array(

			'label'=>'Groups',
			'hierarchical'=>true,
		)

	);
}
add_action('init','portfolio_taxonomy'); */


function portfolio_taxonomy() {
    // Add new taxonomy, make it hierarchical (like categories)
    $labels = array(
        'name'              => _x( 'Groups', 'taxonomy general name', 'textdomain' ),
        'singular_name'     => _x( 'Group', 'taxonomy singular name', 'textdomain' ),
        'search_items'      => __( 'Search Groups', 'textdomain' ),
        'all_items'         => __( 'All Groups', 'textdomain' ),
        'parent_item'       => __( 'Parent Group', 'textdomain' ),
        'parent_item_colon' => __( 'Parent Group:', 'textdomain' ),
        'edit_item'         => __( 'Edit Group', 'textdomain' ),
        'update_item'       => __( 'Update Group', 'textdomain' ),
        'add_new_item'      => __( 'Add New Group', 'textdomain' ),
        'new_item_name'     => __( 'New Group Name', 'textdomain' ),
        'menu_name'         => __( 'Group', 'textdomain' ),
    );

    $args = array(
        'hierarchical'      => true,
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => array( 'slug' => 'Group' ),
    );

    register_taxonomy( 'Group', array( 'portfolio' ), $args );

}

add_action( 'init', 'portfolio_taxonomy', 0 );


























?>