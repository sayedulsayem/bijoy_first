<?php get_header(); ?>

<?php
/*
    Template Name: Home page
 */
?>

<!-- Slider Section -->
<section id="slider">
    <div class="slider-container text-center">
        <div class="overlay">
            <div class="container">

                <div class="slider-txt">
                    <h2 class="title">Our First <span>WordPress Theme</span></h2>
                    <p class="slider-description">Monotonectally formulate superior products with highly efficient infomediaries.
                        Objectively monetize perforices. Globally transition open-source mindshare without high-payoff e-com</p>

                    <div class="link">
                        <a href="#" class="btn custom-btn">View Portfolio</a>
                    </div>
                </div><!-- /.slider-txt -->

            </div><!-- /.container -->
        </div><!-- /.overlay -->
    </div><!-- /.slider-container -->
</section><!-- /#slider -->
<!-- Slider Section End -->


<section id="features">
    <div class="features-section section-padding">
        <div class="container">

            <div class="section-head">
                <h2 class="section-title">Professional Service</h2>
                <p class="section-description">Monotonectally empower fully tested technologies without open-source web services.
                    Uniquely ment after client-centered e-services. Globally transition open-source mindshare without </p>
            </div><!-- /.section-head -->

            <div class="section-content text-center">
                <div class="row">

                    <?php
                    query_posts(array(
                        'post_type' => 'service',
                        'posts_per_page' => 4,
                    ));
                    if (have_posts()): while (have_posts()) : the_post();
                        ?>

                        <div class="col-md-3 col-sm-6">
                            <div class="inner-item">
                                <div class="icon">
                                    <i class="<?php echo get_post_meta($post->ID, 's_icon', true); ?>"></i>
                                </div><!-- /.icon -->

                                <div class="item-info">
                                    <div class="item-title">
                                        <a href="#" class="title"><?php the_title(); ?></a>
                                    </div><!-- /.item-title -->
                                    <p><?php the_content(); ?></p>
                                </div><!-- /.item-info -->
                            </div><!-- /.inner-item -->
                        </div><!-- /.col-md-3 -->
                    <?php
                    endwhile;endif;
                    ?>


                </div><!-- /.row -->
            </div><!-- /.section-content -->
        </div><!-- /.container -->
    </div><!-- /.features-section -->
</section><!-- /#features -->


<section id="team">
    <div class="team-section section-padding">
        <div class="container">

            <div class="section-head">
                <h2 class="section-title">Our Team</h2>
                <p class="section-description">Monotonectally empower fully tested technologies without open-source web services.
                    Uniquely ment after client-centered e-services. Globally transition open-source mindshare without </p>
            </div><!-- /.section-head -->

            <div class="section-content text-center">
                <div class="row">

                    <?php
                    query_posts(array(
                        'post_type' => 'team',
                        'posts_per_page' => 4,
                    ));
                    if (have_posts()): while (have_posts()) : the_post();
                        ?>

                        <div class="col-md-3 col-sm-6">
                            <div class="inner-item">
                                <div class="member-img">
                                    <?php
                                    the_post_thumbnail();
                                    ?>
                                </div>
                                <div class="member-info">
                                    <div class="name">
                                        <a href="#" class="title"><?php the_title(); ?></a>
                                    </div><!-- /.item-title -->
                                    <p class="designation"><?php the_content(); ?></p>
                                </div><!-- /.item-info -->
                            </div><!-- /.inner-item -->
                        </div><!-- /.col-md-3 -->

                    <?php
                    endwhile;
                    endif;
                    ?>

                </div><!-- /.row -->
            </div><!-- /.section-content -->
        </div><!-- /.container -->
    </div><!-- /.team-section -->
</section><!-- /#team -->


<!-- Portfolio Section -->
<section id="portfolio" class="portfolio-section section-padding">
    <div class="container">

        <div class="section-head">
            <h3 class="section-title">Portfolio</h3>
            <p class="section-description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vehicula sapien orci,
                ac lobortis turpis. Sed justo neque, imperdiet ac libero ut, aliquam finibus </p>
        </div><!-- /.section-head -->

        <div id="portfolio-container" class="clearfix">
            <div class="PortfolioFilter text-center">
                <a href="#" data-filter="" class="current">Show All</a>

                <?php

                $terms = get_terms('Group');
                foreach($terms as $term){

                    echo "<a href=\"#\" data-filter=\".$term->slug\">$term->name</a>";
                }

                ?>

            </div>

            <div class="portfolio-items element-from-bottom">
                <?php

                query_posts(array(

                    'post_type'=>'portfolio',
                    'taxonomy'=>'Group',
                ));
                if(have_posts()) : while(have_posts()) : the_post();
                    ?>
                    <?php

                    $terms = get_the_terms( get_the_ID(), 'Group' );

                    if ( $terms && ! is_wp_error( $terms ) ) :

                        $draught_links = array();

                        foreach ( $terms as $term ) {
                            $draught_links[] = $term->slug;
                        }

                        $on_draught = join( " ", $draught_links );

                        ?>
                        <figure class="item <?php echo $on_draught;?> single-item">
                            <div class="portfolio-img">
                                <?php the_post_thumbnail();?>
                            </div><!-- /.portfolio-img -->
                        </figure>
                    <?php endif;?>
                <?php endwhile; endif;?>

            </div><!-- /.portfolio-items -->
        </div><!-- /#portfolio-container -->
    </div><!-- /.container -->
</section><!-- #portfolio -->


<!-- Pricint Section -->
<section id="pricing" class="pricing-section section-padding text-center">
    <div class="container">
        <div class="section-head">
            <h3 class="section-title">Our Pricing</h3>
            <p class="section-description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vehicula sapien orci,
                ac lobortis turpis. Sed justo neque, imperdiet ac libero ut, aliquam finibus </p>
        </div><!-- /.section-head -->

        <div class="section-content">

            <?php
            query_posts(array(
                    'post_type' => 'pricing',
                    'post_per_page' => 4,
            ));

            if (have_posts()) : while(have_posts()) : the_post();

            ?>

            <div class="col-md-3 col-sm-6">
                <div class="inner-item">
                    <div class="item-title">
                        <h3><?php echo get_post_meta($post->ID,'pricing_name',true); ?></h3>
                    </div>
                    <div class="pricing-range casablanca">
                        <p class="currency-number">
                            <span class="currency"><?php echo get_post_meta($post->ID,'currency',true); ?></span>
                            <span class="number"><?php echo get_post_meta($post->ID,'price',true); ?></span>
                        </p>
                        <p class="duration">Per Year</p>
                    </div><!-- /.pricing-range -->
                    <div class="list-wrap">
                        <ul class="pricing-list">
                            <li><a href="#">10 Pages Website Design</a></li>
                            <li><a href="#">Free 1 Year Domain</a></li>
                            <li><a href="#">10 GB Free Hosting</a></li>
                            <li><a href="#">Unlimited Revision</a></li>
                            <li><a href="#">3 Months Support</a></li>
                        </ul>

                        <div class="link">
                            <a href="#" class="btn custom-btn casablanca">Order Now</a>
                        </div>
                    </div>
                </div><!-- /.inner-item -->
            </div><!-- /.col-md-3 -->

            <?php endwhile; endif;?>

        </div><!-- /.section-content -->
    </div>
</section><!-- /#pricing -->
<!-- Pricint Section End -->

<?php get_footer(); ?>
