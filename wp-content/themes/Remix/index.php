<?php get_header(); ?>
<?php
/*
	Template Name: Remix Home page
 */
?>
		<!-- Slider Section -->
		<section id="slider">
			<div class="slider-container">
				<div class="overlay">
					<div id="main-slider" class="carousel slide" data-ride="carousel">
						<div class="container">
							<!-- Indicators -->
							<ol class="carousel-indicators">
						    	<li data-target="#main-slider" data-slide-to="0" class="active"></li>
						    	<li data-target="#main-slider" data-slide-to="1"></li>
						    	<li data-target="#main-slider" data-slide-to="2"></li>
						    	<!-- <li data-target="#main-slider" data-slide-to="3"></li> -->
							</ol>

						  	<!-- Wrapper for slides -->
						  	<div class="carousel-inner" role="listbox">
							    <div class="item active">
								    <div class="slider-item-container wow fadeOutUp" data-wow-duration=".8s" data-wow-delay="6.6s">
								    	<div class="col-md-6 col-sm-6">
									     	<div class="row">
										    	<div class="slider-txt">
													<h2 class="title wow fadeInDown" data-wow-duration=".5s" data-wow-delay="2.5s">welcome to remix</h2>
													<h3 class="sub-title wow fadeInLeft" data-wow-duration=".5s" data-wow-delay="3.9s">To Grow Your Business</h3>
													<p class="description wow fadeInUp" data-wow-duration=".5s" data-wow-delay="3s">Seamlessly engineer wireless technology with inexpensive growth strategies. Globally customize cross-unit channels and progressive growth strategies.</p>

													<a href="#" class="btn btn-default wow zoomIn" data-wow-duration=".5s" data-wow-delay="4.5s">Get Started</a>
												</div><!-- /.slider-txt -->
											</div>
									    </div><!-- /.col-md-6 -->

									    <div class="col-md-6 col-sm-6">
									     	<div class="row">
										    	<div class="slider-<?php echo get_template_directory_uri(); ?>/images">
													<div class="big-img1 wow fadeInUp" data-wow-duration=".5s" data-wow-delay="0.9s"><img src="<?php echo get_template_directory_uri(); ?>/images/slider/slider-img1.png" alt="Bulb Iamge"></div>
												</div><!-- /.slider-<?php echo get_template_directory_uri(); ?>/images -->
											</div>
									    </div><!-- /.col-md-6 -->
									</div><!-- /.slider-item-container -->
							    </div><!-- /.item -->

							    <div class="item">
								    <div class="slider-item-container wow fadeOutUp" data-wow-duration=".6s" data-wow-delay="6.4s">
									    <div class="col-md-6 col-sm-6">
									     	<div class="row">
										    	<div class="slider-txt">
													<h2 class="title wow fadeInUp" data-wow-duration=".5s" data-wow-delay="2.8s">Creative Design</h2>
													<h3 class="sub-title wow fadeInUp" data-wow-duration=".5s" data-wow-delay="3.4s">To Grow Your Business</h3>
													<p class="description wow fadeInUp" data-wow-duration=".5s" data-wow-delay="3.9s">Seamlessly engineer wireless technology with inexpensive growth strategies. Globally customize cross-unit channels and progressive growth strategies.</p>

													<a href="#" class="btn btn-default wow fadeInUp" data-wow-duration=".5s" data-wow-delay="4.5s">Get Started</a>
												</div><!-- /.slider-txt -->
											</div>
									    </div><!-- /.col-md-6 -->

									    <div class="col-md-6 col-sm-6">
									     	<div class="row">
										    	<div class="slider-<?php echo get_template_directory_uri(); ?>/images">
													<div class="small-img wow fadeInDown" data-wow-duration=".5s" data-wow-delay="1.5s">
														<img src="<?php echo get_template_directory_uri(); ?>/images/slider/slider-circle.png" alt="Bulb Iamge">
														<span class="flaticon-light102 wow zoomIn" data-wow-duration=".5s" data-wow-delay="2.3s"></span>
													</div>
													<div class="big-img2 wow fadeInUp" data-wow-duration=".5s" data-wow-delay="0.9s"><img src="<?php echo get_template_directory_uri(); ?>/images/slider/slider-img2.png" alt="Bulb Iamge"></div>
												</div><!-- /.slider-<?php echo get_template_directory_uri(); ?>/images -->
											</div>
									    </div><!-- /.col-md-6 -->
									</div><!-- /.slider-item-container -->
							    </div>

							    <!-- <div class="item">
							    	<div class="col-md-6">
								     	<div class="row">
									    	<div class="slider-txt">
												<h2 class="title wow fadeInDown" data-wow-duration=".5s" data-wow-delay="2s">Responsive Layout</h2>
												<h3 class="sub-title wow fadeInDown" data-wow-duration=".5s" data-wow-delay="3s">to grow your business website template</h3>
												<p class="description wow fadeInUp" data-wow-duration=".5s" data-wow-delay="2.6s">Seamlessly engineer wireless technology with inexpensive growth strategies. Globally customize cross-unit channels and progressive growth strategies.</p>

												<a href="#" class="btn btn-default wow zoomIn" data-wow-duration=".5s" data-wow-delay="4s">Get Started</a>
											</div>
										</div>
								    </div>

								    <div class="col-md-6">
								     	<div class="row">
									    	<div class="slider-<?php echo get_template_directory_uri(); ?>/images">
												<div class="big-img3 wow fadeInUp" data-wow-duration=".5s" data-wow-delay="0.9s"><img src="<?php echo get_template_directory_uri(); ?>/images/slider/slider-img3.png" alt="Bulb Iamge"></div>
											</div>
										</div>
								    </div>
							    </div> -->

							    <div class="item">
								    <div class="slider-item-container wow fadeOutUp" data-wow-duration=".6s" data-wow-delay="6.6s">
								    	<div class="col-md-6 col-sm-6">
									     	<div class="row">
										    	<div class="slider-txt">
													<h2 class="title wow fadeInLeft" data-wow-duration=".5s" data-wow-delay="2s">Money Back Guarantee</h2>
													<h3 class="sub-title wow fadeInRight" data-wow-duration=".5s" data-wow-delay="2.6s">to grow your business website</h3>
													<p class="description wow fadeInLeft" data-wow-duration=".5s" data-wow-delay="3s">Seamlessly engineer wireless technology with inexpensive growth strategies. Globally customize cross-unit channels and progressive growth strategies.</p>

													<a href="#" class="btn btn-default wow fadeInRight" data-wow-duration=".5s" data-wow-delay="4s">Get Started</a>
												</div><!-- /.slider-txt -->
											</div>
									    </div><!-- /.col-md-6 -->

									    <div class="col-md-6 col-sm-6">
									     	<div class="row">
										    	<div class="slider-<?php echo get_template_directory_uri(); ?>/images money">
													<div class="big-img4 wow fadeInUp" data-wow-duration=".5s" data-wow-delay="0.9s"><img src="<?php echo get_template_directory_uri(); ?>/images/slider/slider-img4.png" alt="Slider Iamge"></div>
												</div><!-- /.slider-<?php echo get_template_directory_uri(); ?>/images -->
											</div>
									    </div><!-- /.col-md-6 -->
									</div><!-- /.slider-item-container -->
							    </div>

						  	</div><!-- /.carousel-inner -->

							<!-- Controls -->
							<a class="slide-arow left" href="#main-slider" role="button" data-slide="prev">
								<em></em>
								<span></span>
							</a>
							<a class="slide-arow right" href="#main-slider" role="button" data-slide="next">
								<em></em>
								<span></span>
							</a>
						</div><!-- /.container -->
					</div><!-- /#main-slider -->
				</div><!-- /.overlay -->
			</div><!-- /.slider-container -->
		</section><!-- /#slider -->
		<!-- Slider Section End -->




		<section id="about">
			<div class="about-section section-padding">
				<div class="container">
					<div class="section-head">
						<h2 class="section-title">
							<?php echo cs_get_option('about_sec_title'); ?>
						</h2>
						<p class="section-description">
                            <?php echo cs_get_option('about_sec_des'); ?>
						</p>
					</div><!-- /.section-head -->
					<div class="section-content">
						<div class="row">

							<div class="col-lg-4">
								<div class="item-md4 wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".6s">
									<h3><?php echo cs_get_option('about_sec_s_title'); ?></h3>
									<p class="item-description"><?php echo cs_get_option('about_sec_s_des'); ?></p>

                                    <?php
                                    $group= cs_get_option('about_f_group');
                                    foreach ($group as $item=>$value){ ?>
                                        <div class="inner-item">
										<div class="icon">
											<i class="<?php echo $value['AUgroup_icon_change'];?>"></i>
										</div><!-- /.icon -->
										<div class="item-info">
											<h4 class="item-title">
												<a href="#"><?php echo $value['AUgroup_title_change'];?></a>
											</h4><!-- /.item-title -->
											<p><?php echo $value['AUgroup_des_change'];?></p>
										</div><!-- /.item-info -->
									</div><!-- /.inner-item -->
                                    <?php
                                    }
                                    ?>

								</div><!-- /.item-md4 -->
							</div><!-- /.col-md-3 -->

                            <?php
                            $group2= cs_get_option('about_s_group');
                            foreach ($group2 as $item=>$value){
                            ?>
							<div class="col-lg-4">
								<div class="item-md4 main-blog wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".9s">
									<div class="post-img">
										<img class="aboutImage" src="<?php echo $value['AUgroup2_image_change']; ?>" alt="post Image">
									</div>
									<div class="item-info">
										<h4 class="item-title">
											<a href="#"><?php echo $value['AUgroup2_title_change']; ?></a>
										</h4><!-- /.item-title -->
										<p><?php echo $value['AUgroup2_des_change']; ?></p>
									</div><!-- /.item-info -->
									<a href="<?php echo $value['AUgroup2_link_change']; ?>" class="btn btn-default"><?php echo $value['AUgroup2_button_change']; ?></a>
								</div><!-- /.item-md4 -->
							</div><!-- /.col-md-3 -->
                            <?php
                            }
                            ?>

						</div><!-- /.row -->
					</div><!-- /.section-content -->
				</div><!-- /.container -->
			</div><!-- /.about-section -->
		</section><!-- /#about -->


		<!-- Portfolio Section -->
		<section id="portfolio-work" class="portfolio-work dark-bg">
			<div class="container">

				<div class="section-head">
					<h2 class="section-title">
						Our Portfolio
					</h2>
				</div><!-- /.section-head -->
				
				<div id="portfolio-container" class="clearfix">
					<div class="portfolioFilter">
						<a href="#" data-filter="" class="current">All</a>
						<a href="#" data-filter=".design">Web Design</a>
						<a href="#" data-filter=".development">Development</a>
						<a href="#" data-filter=".graphic">Graphic</a>
						<a href="#" data-filter=".wordPress">WordPress</a>
					</div> <!-- /.portfolioFilter -->

					<div class="portfolio-item element-from-bottom">
						<figure class="item item-w2 wordPress development">
							<img src="<?php echo get_template_directory_uri(); ?>/images/portfolio/01.jpg" alt="Item 1" />
							<div class="overlay"></div>
							<div class="expand">
								<div class="expand-wrap">
									<div class="expand-info">
										<h3><a href="#">Web Development</a></h3>
										<p>Seamlessly incentivize error-free core Seamlessly incentivize </p>
									</div><!-- /.expand-info -->
									<a href="<?php echo get_template_directory_uri(); ?>/images/portfolio/01.jpg" class="boxer" title="Portfolio Image Title">
										<span class="flaticon-zoom10"></span>
									</a>
								</div><!-- /.expand-wrap -->
							</div>
						</figure>

						<figure class="item design wordPress">
							<img src="<?php echo get_template_directory_uri(); ?>/images/portfolio/02.jpg" alt="Item 2" />
							<div class="overlay"></div>
							<div class="expand">
								<div class="expand-info">
									<h3><a href="#">Web Design</a></h3>
									<p>Seamlessly incentivize error-free core Seamlessly incentivize </p>
								</div><!-- /.expand-info -->
								<a href="<?php echo get_template_directory_uri(); ?>/images/portfolio/02.jpg" class="boxer" title="Portfolio Image Title">
									<span class="flaticon-zoom10"></span>
								</a>
							</div>
						</figure>

						<figure class="item item-h2 wordPress development">
							<img src="<?php echo get_template_directory_uri(); ?>/images/portfolio/03.jpg" alt="Item 3" />
							<div class="overlay"></div>
							<div class="expand">
								<div class="expand-info">
									<h3><a href="#">Development</a></h3>
									<p>Seamlessly incentivize error-free core Seamlessly incentivize </p>
								</div><!-- /.expand-info -->
								<a href="<?php echo get_template_directory_uri(); ?>/images/portfolio/03.jpg" class="boxer" title="Portfolio Image Title">
									<span class="flaticon-zoom10"></span>
								</a>
							</div>
						</figure>

						<figure class="item design wordPress">
							<img src="<?php echo get_template_directory_uri(); ?>/images/portfolio/04.jpg" alt="Item 4" />
							<div class="overlay"></div>
							<div class="expand">
								<div class="expand-info">
									<h3><a href="#">Graphic</a></h3>
									<p>Seamlessly incentivize error-free core Seamlessly incentivize </p>
								</div><!-- /.expand-info -->
								<a href="<?php echo get_template_directory_uri(); ?>/images/portfolio/04.jpg" class="boxer" title="Portfolio Image Title">
									<span class="flaticon-zoom10"></span>
								</a>
							</div>
						</figure>

						<figure class="item development graphic">
							<img src="<?php echo get_template_directory_uri(); ?>/images/portfolio/05.jpg" alt="Item 5" />
							<div class="overlay"></div>
							<div class="expand">
								<div class="expand-info">
									<h3><a href="#">WordPress</a></h3>
									<p>Seamlessly incentivize error-free core Seamlessly incentivize </p>
								</div><!-- /.expand-info -->
								<a href="<?php echo get_template_directory_uri(); ?>/images/portfolio/05.jpg" class="boxer" title="Portfolio Image Title">
									<span class="flaticon-zoom10"></span>
								</a>
							</div>
						</figure>

						<figure class="item wordPress development">
							<img src="<?php echo get_template_directory_uri(); ?>/images/portfolio/06.jpg" alt="Item 6" />
							<div class="overlay"></div>
							<div class="expand">
								<div class="expand-info">
									<h3><a href="#">Print Design</a></h3>
									<p>Seamlessly incentivize error-free core Seamlessly incentivize </p>
								</div><!-- /.expand-info -->
								<a href="<?php echo get_template_directory_uri(); ?>/images/portfolio/06.jpg" class="boxer" title="Portfolio Image Title">
									<span class="flaticon-zoom10"></span>
								</a>
							</div>
						</figure>

						<figure class="item item-w2 design graphic">
							<img src="<?php echo get_template_directory_uri(); ?>/images/portfolio/07.jpg" alt="Item 7" />
							<div class="overlay"></div>
							<div class="expand">
								<div class="expand-info">
									<h3><a href="#">WordPress</a></h3>
									<p>Seamlessly incentivize error-free core Seamlessly incentivize </p>
								</div><!-- /.expand-info -->
								<a href="<?php echo get_template_directory_uri(); ?>/images/portfolio/07.jpg" class="boxer" title="Portfolio Image Title">
									<span class="flaticon-zoom10"></span>
								</a>
							</div>
						</figure>

						<figure class="item cat-2 development graphic">
							<img src="<?php echo get_template_directory_uri(); ?>/images/portfolio/08.jpg" alt="Item 8" />
							<div class="overlay"></div>
							<div class="expand">
								<div class="expand-info">
									<h3><a href="#">Development</a></h3>
									<p>Seamlessly incentivize error-free core Seamlessly incentivize </p>
								</div><!-- /.expand-info -->
								<a href="<?php echo get_template_directory_uri(); ?>/images/portfolio/08.jpg" class="boxer" title="Portfolio Image Title">
									<span class="flaticon-zoom10"></span>
								</a>
							</div>
						</figure>

						<figure class="item cat-2 design wordPress">
							<img src="<?php echo get_template_directory_uri(); ?>/images/portfolio/09.jpg" alt="Item 9" />
							<div class="overlay"></div>
							<div class="expand">
								<div class="expand-info">
									<h3><a href="#">Graphic</a></h3>
									<p>Seamlessly incentivize error-free core Seamlessly incentivize </p>
								</div><!-- /.expand-info -->
								<a href="<?php echo get_template_directory_uri(); ?>/images/portfolio/09.jpg" class="boxer" title="Portfolio Image Title">
									<span class="flaticon-zoom10"></span>
								</a>
							</div>
						</figure>
					</div><!-- /.portfolio-item -->
					
					<div class="link">
						<a href="#" class="btn btn-default">Load More</a>
					</div>

				</div><!-- /#portfolio-container -->
			</div>
		</section><!-- #portfolio -->



        <div id="add-section" class="add-section"> 
	        <div class="overlay">
		        <div class="section-padding">
		            <div class="container">                
		                <div class="section-content text-center wow zoomIn" data-wow-duration=".5s" data-wow-delay=".6s">
		                   <h2>Remix is a awesome psd template</h2>
		                   <p class="section-description">Appropriately customize real-time expertise through adaptive human capital. Competently predominate standardized functionalities with excellent alignments.</p>

		                    <div class="link">
								<a href="#" class="btn btn-default">Get Started</a>
							</div>
		                </div><!-- /.section-content --> 
		            </div> 
		        </div><!-- /.section-padding -->
	        </div>
        </div><!-- /#add-section -->


        <section id="services" class="service-section">
	        <div class="section-padding">
	            <div class="container"> 

	            	<div class="section-head wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".6s">
						<h2 class="section-title">
							<?php echo cs_get_option('service_title'); ?>
						</h2>
						<p class="section-description">
                            <?php echo cs_get_option('service_des'); ?>
						</p>
					</div><!-- /.section-head -->


	                <div class="section-content">
	                   <div class="row">

                           <?php
                           $serviceList=cs_get_option('service_list');
                           foreach ($serviceList as $item=>$value){
                               ?>
                               <div class="col-md-3 col-sm-6">
                                   <div class="item-md3 wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".9s">
                                       <div class="<?php echo $value['service_icon']; ?>"></div>
                                       <div class="item-info">
                                           <h4 class="item-title">
                                               <a href="#"><?php echo $value['service_title']; ?></a>
                                           </h4><!-- /.item-title -->
                                           <p><?php echo $value['service_des']; ?></p>
                                       </div>
                                   </div><!-- /.item-md3 -->
                               </div><!-- /.col-md-3 -->
                           <?php } ?>

	                   </div>
	                </div><!-- /.section-content --> 
	            </div> 
	        </div><!-- /.section-padding -->
        </section><!-- /#services -->



		<section id="team" class="team-section dark-bg">
			<div class="section-padding">
				<div class="container">
					
					<div class="section-head">
						<h2 class="section-title">
							Meet Our Team
						</h2>
						<p class="section-description">
							Phosfluorescently visualize resource maximizing quality vectorAuthoritativelyPhosfluorescently visualize resource maximizing quality vectorAuthoritatively
						</p>
					</div><!-- /.section-head -->


					<div class="section-content">
						<div class="row">

							<div class="col-md-3 col-sm-6">
								<div class="item-md3 wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".6s">
									<div class="team-img">
										<img src="<?php echo get_template_directory_uri(); ?>/images/team/01.jpg" alt="Team image">
										<div class="overlay"></div>
										<div class="social-icon">
											<a href="#"><i class="fa fa-facebook"></i></a>
											<a href="#"><i class="fa fa-twitter"></i></a>
											<a href="#"><i class="fa fa-google-plus"></i></a>
										</div>
									</div><!-- /.icon -->

									<div class="member-info">
										<h4 class="name item-title">
											<a href="#">Karls Vinson</a>
										</h4>
										<div class="designation">
											Head Of idea
										</div><!-- /.item-title -->
										<p>Tatively scale ubiquitous manufaconve niently create virtual supply .Efficiently productize technically</p>
									</div><!-- /.item-info -->
								</div><!-- /.item-md3 -->
							</div><!-- /.col-md-3 -->

							<div class="col-md-3 col-sm-6">
								<div class="item-md3 wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".9s">
									<div class="team-img">
										<img src="<?php echo get_template_directory_uri(); ?>/images/team/02.jpg" alt="Team image">
										<div class="overlay"></div>
										<div class="social-icon">
											<a href="#"><i class="fa fa-facebook"></i></a>
											<a href="#"><i class="fa fa-twitter"></i></a>
											<a href="#"><i class="fa fa-google-plus"></i></a>
										</div>
									</div><!-- /.icon -->
									<div class="member-info">
										<h4 class="name item-title">
											<a href="#">Ailsa Craig</a>
										</h4>
										<div class="designation">
											Project Manager
										</div><!-- /.item-title -->
										<p>Tatively scale ubiquitous manufaconve niently create virtual supply .Efficiently productize technically</p>
									</div><!-- /.item-info -->
								</div><!-- /.item-md3 -->
							</div><!-- /.col-md-3 -->

							<div class="col-md-3 col-sm-6">
								<div class="item-md3 wow fadeInUp" data-wow-duration=".5s" data-wow-delay="1.2s">
									<div class="team-img">
										<img src="<?php echo get_template_directory_uri(); ?>/images/team/03.jpg" alt="Team image">
										<div class="overlay"></div>
										<div class="social-icon">
											<a href="#"><i class="fa fa-facebook"></i></a>
											<a href="#"><i class="fa fa-twitter"></i></a>
											<a href="#"><i class="fa fa-google-plus"></i></a>
										</div>
									</div><!-- /.icon -->
									<div class="member-info">
										<h4 class="name item-title">
											<a href="#">Vvs Lakhman</a>
										</h4>
										<div class="designation">
											Web Developer
										</div><!-- /.item-title -->
										<p>Tatively scale ubiquitous manufaconve niently create virtual supply .Efficiently productize technically</p>
									</div><!-- /.item-info -->
								</div><!-- /.item-md3 -->
							</div><!-- /.col-md-3 -->

							<div class="col-md-3 col-sm-6">
								<div class="item-md3 wow fadeInUp" data-wow-duration=".5s" data-wow-delay="1.5s">
									<div class="team-img">
										<img src="<?php echo get_template_directory_uri(); ?>/images/team/04.jpg" alt="Team image">
										<div class="overlay"></div>
										<div class="social-icon">
											<a href="#"><i class="fa fa-facebook"></i></a>
											<a href="#"><i class="fa fa-twitter"></i></a>
											<a href="#"><i class="fa fa-google-plus"></i></a>
										</div>
									</div><!-- /.icon -->
									<div class="member-info">
										<h4 class="name item-title">
											<a href="#">Anwen lieoni</a>
										</h4>
										<div class="designation">
											Graphic  & UI/UX Designer
										</div><!-- /.item-title -->
										<p>Tatively scale ubiquitous manufaconve niently create virtual supply .Efficiently productize technically</p>
									</div><!-- /.item-info -->
								</div><!-- /.item-md3 -->
							</div><!-- /.col-md-3 -->
						</div><!-- /.row -->
					</div><!-- /.section-content -->
				</div><!-- /.container -->
			</div><!-- /.team-section -->
		</section><!-- /#team -->



		<section id="pricing" class="pricing-section">
			<div class="section-padding">
				<div class="container">
					
					<div class="section-head wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".6s">
						<h2 class="section-title">
							Our Pricing Plan
						</h2>
						<p class="section-description">
							Phosfluorescently visualize resource maximizing quality vectorAuthoritativelyPhosfluorescently visualize resource maximizing quality vectorAuthoritatively
						</p>
					</div><!-- /.section-head -->

					<div class="section-content text-center">
						<div class="row">

							<div class="col-md-3 col-sm-6">
								<div class="item-md3 wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".6s">
									<div class="item-head">
										<h4 class="title">Basic</h4>

										<div class="currency-price-time">
											<span class="currency"><sub>$</sub></span> 
											<span class="number">19</span>
											<span class="duration"><sup>/Month</sup></span>
										</div>
									</div>

									<div class="item-list text-left">
										<ul class="list1">
											<li>Unlimited Products</li>
											<li>500 GB Bandwidth</li>
											<li>24/7 Customer Support</li>
										</ul>
										<ul class="list2">
											<li>20 Pages, Galleries</li>
											<li>Unlimited Products</li>
											<li>Custom Domain free</li>
											<li>Mobile Website and store</li>
										</ul>
									</div>
									<div class="link">
										<a href="#" class="btn btn-default">Buy Now</a>
									</div>
								</div><!-- /.item-md3 -->
							</div><!-- /.col-md-3 -->

							<div class="col-md-3 col-sm-6">
								<div class="item-md3 wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".9s">
									<div class="item-head bg2">
										<h4 class="title">Standard</h4>

										<div class="currency-price-time">
											<span class="currency"><sub>$</sub></span> 
											<span class="number">33</span>
											<span class="duration"><sup>/Month</sup></span>
										</div>
									</div>

									<div class="item-list text-left">
										<ul class="list1">
											<li>Unlimited Products</li>
											<li>500 GB Bandwidth</li>
											<li>24/7 Customer Support</li>
											<li>20 Pages, Galleries</li>
										</ul>
										<ul class="list2">
											<li>Unlimited Products</li>
											<li>Custom Domain free</li>
											<li>Mobile Website and store</li>
										</ul>
									</div>
									<div class="link">
										<a href="#" class="btn btn-default">Buy Now</a>
									</div>
								</div><!-- /.item-md3 -->
							</div><!-- /.col-md-3 -->

							<div class="col-md-3 col-sm-6">
								<div class="item-md3 wow fadeInUp" data-wow-duration=".5s" data-wow-delay="1.2s">
									<div class="item-head bg3">
										<h4 class="title">Professional</h4>

										<div class="currency-price-time">
											<span class="currency"><sub>$</sub></span> 
											<span class="number">49</span>
											<span class="duration"><sup>/Month</sup></span>
										</div>
									</div>

									<div class="item-list text-left">
										<ul class="list1">
											<li>Unlimited Products</li>
											<li>500 GB Bandwidth</li>
											<li>24/7 Customer Support</li>
											<li>20 Pages, Galleries</li>
											<li>Unlimited Products</li>
										</ul>
										<ul class="list2">
											<li>Custom Domain free</li>
											<li>Mobile Website and store</li>
										</ul>
									</div>
									<div class="link">
										<a href="#" class="btn btn-default">Buy Now</a>
									</div>
								</div><!-- /.item-md3 -->
							</div><!-- /.col-md-3 -->

							<div class="col-md-3 col-sm-6">
								<div class="item-md3 wow fadeInUp" data-wow-duration=".5s" data-wow-delay="1.5s">
									<div class="item-head bg4">
										<h4 class="title">Premium</h4>
										<div class="currency-price-time">
											<span class="currency"><sub>$</sub></span> 
											<span class="number">99</span>
											<span class="duration"><sup>/Month</sup></span>
										</div>
									</div>

									<div class="item-list text-left">
										<ul class="list1">
											<li>Unlimited Products</li>
											<li>500 GB Bandwidth</li>
											<li>24/7 Customer Support</li>
											<li>20 Pages, Galleries</li>
											<li>Unlimited Products</li>
											<li>Custom Domain free</li>
											<li>Mobile Website and store</li>
										</ul>
									</div>
									<div class="link">
										<a href="#" class="btn btn-default">Buy Now</a>
									</div>
								</div><!-- /.item-md3-->
							</div><!-- /.col-md-3 -->

						</div><!-- /.row -->
					</div><!-- /.section-content -->
				</div><!-- /.container -->
			</div><!-- /.pricing-section -->
		</section><!-- /#pricing -->

		<section id="work" class="work-section dark-bg">
			<div class="section-padding">
				<div class="container">

					<div class="section-content">
						<div class="row">
							<div class="col-md-8">
								<div class="section-head wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".3s">
									<h2 class="section-title">
										Working Style
									</h2>
									<p class="section-description">
										Phosfluorescently visualize resource maximizing quality vectorAuthoritativelyPhosfluorescently visualize resource maximizing quality vectorAuthoritatively
									</p>
								</div><!-- /.section-head -->
								<div class="row">
									<div class="col-md-4 col-sm-6">
										<div class="inner-item wow fadeIn" data-wow-duration=".5s" data-wow-delay=".5s">
											<div class="icon flaticon-two205"></div>
											<div class="item-info">
												<h5 class="item-title">
													<a href="#">Meet</a>
												</h5><!-- /.item-title -->
											</div>
										</div><!-- /.inner-item -->
									</div><!-- /.col-md-4 -->

									<div class="col-md-4 col-sm-6">
										<div class="inner-item wow fadeIn" data-wow-duration=".5s" data-wow-delay=".7s">
											<div class="icon flaticon-business112"></div>
											<div class="item-info">
												<h5 class="item-title">
													<a href="#">Plan</a>
												</h5><!-- /.item-title -->
											</div>
										</div><!-- /.inner-item -->
									</div><!-- /.col-md-4 -->

									<div class="col-md-4 col-sm-6">
										<div class="inner-item wow fadeIn" data-wow-duration=".5s" data-wow-delay=".9s">
											<div class="icon flaticon-homework"></div>
											<div class="item-info">
												<h5 class="item-title">
													<a href="#">Design</a>
												</h5><!-- /.item-title -->
											</div>
										</div><!-- /.inner-item -->
									</div><!-- /.col-md-4 -->

									<div class="col-md-4 col-sm-6">
										<div class="inner-item wow fadeIn" data-wow-duration=".5s" data-wow-delay="1.1s">
											<div class="icon flaticon-internet17"></div>
											<div class="item-info">
												<h5 class="item-title">
													<a href="#">Develop</a>
												</h5><!-- /.item-title -->
											</div>
										</div><!-- /.inner-item -->
									</div><!-- /.col-md-4 -->

									<div class="col-md-4 col-sm-6">
										<div class="inner-item wow fadeIn" data-wow-duration=".5s" data-wow-delay="1.3s">
											<div class="icon flaticon-book140"></div>
											<div class="item-info">
												<h5 class="item-title">
													<a href="#">Testing</a>
												</h5><!-- /.item-title -->
											</div>
										</div><!-- /.inner-item -->
									</div><!-- /.col-md-4 -->

									<div class="col-md-4 col-sm-6">
										<div class="inner-item wow fadeIn" data-wow-duration=".5s" data-wow-delay="1.5s">
											<div class="icon flaticon-delivery13"></div>
											<div class="item-info">
												<h5 class="item-title">
													<a href="#">Launch</a>
												</h5><!-- /.item-title -->
											</div>
										</div><!-- /.inner-item -->
									</div><!-- /.col-md-4 -->
								</div><!-- /.row -->
							</div><!-- /.col-md-8 -->

							<div class="col-md-4">
								<div class="style-img wow fadeInDown" data-wow-duration=".5s" data-wow-delay="2s">
									<img src="<?php echo get_template_directory_uri(); ?>/images/style.png" alt="Style Image">
								</div><!-- /.style-img -->
							</div><!-- /.col-md-4 -->
						</div><!-- /.row -->
					</div><!-- /.section-content -->
				</div><!-- /.container -->
			</div><!-- /.work-section -->
		</section><!-- /#work -->

		<section id="blog" class="blog-section">
			<div class="section-padding">
				<div class="container">
					
					<div class="section-head wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".3s">
						<h2 class="section-title">
							Our Blog
						</h2>
						<p class="section-description">
							Phosfluorescently visualize resource maximizing quality vectorAuthoritativelyPhosfluorescently visualize resource maximizing quality vectorAuthoritatively
						</p>
					</div><!-- /.section-head -->

					<div class="section-content">
						<div class="row">

							<div class="col-md-4">
								<div class="item-md4 wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".3s">
									<div class="item-md4-head">
										<div class="date text-center">
											<span>30</span>
											sep
										</div>
										<h5 class="entry-title"><a href="#">Seamlessly redefstanards  top customer.</a></h5>
										<p class="entry-meta">
											<a href="#"><i class="fa fa-user"></i> Malek 1316</a> <span>|</span>
											<a href="#"><i class="fa fa-comments"></i> 05 Comments</a> <span>|</span>
											<a href="#"><i class="fa fa-heart-o"></i> 155</a>
										</p>
									</div><!-- /.item-md4-head -->

									<div class="thumbnail-img">
										<img src="<?php echo get_template_directory_uri(); ?>/images/post/03.jpg" alt="Post Iamge">
									</div><!-- /.thumbnail-img -->
									<div class="entry-content">
										<p>error-free strategic theme aret-of-the-box imperatives. Progressively administratdologies with scalable infomediaries. Assertively</p>
									</div>
									<div class="link">
										<a href="#" class="btn btn-default">Read More</a>
									</div>
								</div><!-- /.item-md4 -->
							</div><!-- /.col-md-4 -->

							<div class="col-md-4">
								<div class="item-md4 wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".5s">
									<div class="item-md4-head">
										<div class="date text-center">
											<span>30</span>
											sep
										</div>
										<h5 class="entry-title"><a href="#">Seamlessly redefstanards  top customer.</a></h5>
										<p class="entry-meta">
											<a href="#"><i class="fa fa-user"></i> Malek 1316</a> <span>|</span>
											<a href="#"><i class="fa fa-comments"></i> 05 Comments</a> <span>|</span>
											<a href="#"><i class="fa fa-heart-o"></i> 155</a>
										</p>
									</div><!-- /.item-md4-head -->
									
									<div class="thumbnail-img">
										<img src="<?php echo get_template_directory_uri(); ?>/images/post/04.jpg" alt="Post Iamge">
									</div><!-- /.thumbnail-img -->
									<div class="entry-content">
										<p>error-free strategic theme aret-of-the-box imperatives. Progressively administratdologies with scalable infomediaries. Assertively</p>
									</div>
									<div class="link">
										<a href="#" class="btn btn-default">Read More</a>
									</div>
								</div><!-- /.item-md4 -->
							</div><!-- /.col-md-4 -->

							<div class="col-md-4">
								<div class="item-md4 wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".7s">
									<div class="item-md4-head">
										<div class="date text-center">
											<span>30</span>
											sep
										</div>
										<h5 class="entry-title"><a href="#">Seamlessly redefstanards  top customer.</a></h5>
										<p class="entry-meta">
											<a href="#"><i class="fa fa-user"></i> Malek 1316</a> <span>|</span>
											<a href="#"><i class="fa fa-comments"></i> 05 Comments</a> <span>|</span>
											<a href="#"><i class="fa fa-heart-o"></i> 155</a>
										</p>
									</div><!-- /.item-md4-head -->
									
									<div class="thumbnail-img">
										<img src="<?php echo get_template_directory_uri(); ?>/images/post/05.jpg" alt="Post Iamge">
									</div><!-- /.thumbnail-img -->
									<div class="entry-content">
										<p>error-free strategic theme aret-of-the-box imperatives. Progressively administratdologies with scalable infomediaries. Assertively</p>
									</div>
									<div class="link">
										<a href="#" class="btn btn-default">Read More</a>
									</div>
								</div><!-- /.item-md4 -->
							</div><!-- /.col-md-4 -->

						</div><!-- /.row -->
					</div><!-- /.section-content -->
				</div><!-- /.container -->
			</div><!-- /.blog-section -->
		</section><!-- /#blog -->

		<!-- Video Section -->
		<section id="video" class="video-section">
			<div class="overlay">
				<div class="section-padding">
					<div class="section-content text-center">
						<a href="https://www.youtube.com/embed/didkQXhjeVQ" class="boxer" title="Creative Video">
							<!-- <img class="wow bounceIn animated" data-wow-delay="0.6s" src="<?php echo get_template_directory_uri(); ?>/images/play.png" alt="Play Image"> -->
							<em class="flaticon-musicplayer7 wow rollIn animated" data-wow-delay="0.6s"></em>
						</a>
					</div>
				</div>
			</div><!-- /.overlay -->
		</section><!-- /#video -->
		<!-- Video Section End -->

		<section id="client" class="client-section dark-bg">
			<div class="section-padding">
				<div class="container">
					
					<div class="section-head">
						<h2 class="section-title">
							Happy Clients & Sponsor
						</h2>
					</div><!-- /.section-head -->

					<div class="section-content">
						<div class="row">

							<div class="col-md-6">
								<div id="clients-slider-container">
									<div class="parallax">

										<div id="client-slider" class="carousel slide" data-ride="carousel">
											<!-- Indicators -->
											<ol class="carousel-indicators">
											    <li data-target="#client-slider" data-slide-to="0" class="active"></li>
											    <li data-target="#client-slider" data-slide-to="1"></li>
											    <li data-target="#client-slider" data-slide-to="2"></li>
											</ol>

											<!-- Wrapper for slides -->
											<div class="carousel-inner" role="listbox">
											    <div class="item active">
											    	<div class="reviewer-img">
											    		<img src="<?php echo get_template_directory_uri(); ?>/images/clients/client1.jpg" alt="Clients Iamge">
											    	</div>
											        <div class="ss-text">
														<p>Interactively synergize effective materials after bleeding-edge niche markets. Holisticly communicate state of the art relationships  systems.</p>
														<a href="#" class="name">Roberts finque</a>
														<a href="#" class="designation">Web Developer</a>
													</div><!-- /.tweet-text -->
											    </div><!-- /.item -->

											    <div class="item">
											    	<div class="reviewer-img">
											    		<img src="<?php echo get_template_directory_uri(); ?>/images/clients/client1.jpg" alt="Clients Iamge">
											    	</div>
											        <div class="ss-text">
														<p>Interactively synergize effective materials after bleeding-edge niche markets. Holisticly communicate state of the art relationships  systems.</p>
														<a href="#" class="name">Roberts finque</a>
														<a href="#" class="designation">Web Developer</a>
													</div><!-- /.tweet-text -->
											    </div><!-- /.item -->

											    <div class="item">
											    	<div class="reviewer-img">
											    		<img src="<?php echo get_template_directory_uri(); ?>/images/clients/client1.jpg" alt="Clients Iamge">
											    	</div>
											      	<div class="ss-text">
														<p>Interactively synergize effective materials after bleeding-edge niche markets. Holisticly communicate state of the art relationships  systems.</p>
														<a href="#" class="name">Roberts finque</a>
														<a href="#" class="designation">Web Developer</a>
													</div><!-- /.tweet-text -->
												</div><!-- /.item -->
											</div><!-- /.carousel-inner -->
										    
										</div><!-- /#client-slider -->
									</div><!-- /.services-overlay -->
								</div><!-- #clients-slider-container -->
							</div><!-- /.col-md-6 -->

							<!-- <div class="angle-line"></div> -->

							<div class="col-md-6">
								<div class="row">
									<div class="client-wrap">
										<div class="col-sm-4">
											<div class="single-client">
												<img src="<?php echo get_template_directory_uri(); ?>/images/clients/02.png" alt="Clients Image">
											</div>
										</div>
										<div class="col-sm-4">
											<div class="single-client">
												<img src="<?php echo get_template_directory_uri(); ?>/images/clients/03.png" alt="Clients Image">
											</div>
										</div>
										<div class="col-sm-4">
											<div class="single-client">
												<img src="<?php echo get_template_directory_uri(); ?>/images/clients/04.png" alt="Clients Image">
											</div>
										</div>
										<div class="col-sm-4">
											<div class="single-client">
												<img src="<?php echo get_template_directory_uri(); ?>/images/clients/07.png" alt="Clients Image">
											</div>
										</div>
										<div class="col-sm-4">
											<div class="single-client">
												<img src="<?php echo get_template_directory_uri(); ?>/images/clients/06.png" alt="Clients Image">
											</div>
										</div>
										<div class="col-sm-4">
											<div class="single-client">
												<img src="<?php echo get_template_directory_uri(); ?>/images/clients/07.png" alt="Clients Image">
											</div>
										</div>
									</div><!-- /.client-wrap -->
								</div>
							</div><!-- /.col-md-4 -->

						</div><!-- /.row -->
					</div><!-- /.section-content -->
				</div><!-- /.container -->
			</div>
		</section><!-- /#client -->
<?php get_footer(); ?>


