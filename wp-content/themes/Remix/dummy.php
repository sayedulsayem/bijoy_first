<?php
$serviceList=cs_get_option('service_list');
foreach ($serviceList as $item=>$value){
    ?>
    <div class="col-md-3 col-sm-6">
        <div class="item-md3 wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".9s">
            <div class="<?php echo $value['service_icon']; ?>"></div>
            <div class="item-info">
                <h4 class="item-title">
                    <a href="#"><?php echo $value['service_title']; ?></a>
                </h4><!-- /.item-title -->
                <p><?php echo $value['service_des']; ?></p>
            </div>
        </div><!-- /.item-md3 -->
    </div><!-- /.col-md-3 -->
<?php } ?>


<div class="col-md-3 col-sm-6">
    <div class="item-md3 wow fadeInRight" data-wow-duration=".5s" data-wow-delay="1.7s">
        <div class="icon flaticon-computer261"></div>
        <div class="item-info">
            <h4 class="item-title">
                <a href="#">Web Design</a>
            </h4><!-- /.item-title -->
            <p>Uniquely maximize equity invested business with maintainable schemas.</p>
        </div>
    </div><!-- /.item-md3 -->
</div><!-- /.col-md-3 -->
