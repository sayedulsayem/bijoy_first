<?php
/**
 * Created by PhpStorm.
 * User: Sayem
 * Date: 12-Nov-17
 * Time: 9:58 PM
 */

require_once ('codestar/cs-framework.php');
register_nav_menus( array(
	'remix_header_menu' => 'remix header menu',
	'remix_footer_menu' => 'remix header menu',
) );

function include_css_js(){

    //wp_enqueue_style('name',get_template_directory_uri().'/file location',array(),'version','all');

    wp_enqueue_style('awesome',get_template_directory_uri().'/assets/css/font-awesome.min.css',array(),'1.0.0','all');
    wp_enqueue_style('flaticon',get_template_directory_uri().'/assets/css/flaticon.css',array(),'1.0.0','all');
    wp_enqueue_style('boxer',get_template_directory_uri().'/assets/css/jquery.fs.boxer.min.css',array(),'1.0.0','all');
    wp_enqueue_style('bootstrap',get_template_directory_uri().'/assets/css/bootstrap.min.css',array(),'1.0.0','all');
    wp_enqueue_style('animate',get_template_directory_uri().'/assets/css/animate.min.css',array(),'1.0.0','all');
    wp_enqueue_style('style',get_template_directory_uri().'/assets/css/style.css',array(),'1.0.0','all');
    wp_enqueue_style('responsive',get_template_directory_uri().'/assets/css/responsive.css',array(),'1.0.0','all');


    //wp_enqueue_script('name',get_template_directory_uri().'/file location',array(),'version,true);

    wp_enqueue_script('modernizr',get_template_directory_uri().'/assets/js/modernizr.js',array('jquery'),'version',true);
    wp_enqueue_script('plugins',get_template_directory_uri().'/assets/js/plugins.js',array('jquery'),'version',true);
    wp_enqueue_script('OnePagenNav',get_template_directory_uri().'/assets/js/OnePagenNav.js',array('jquery'),'version',true);
    wp_enqueue_script('wow',get_template_directory_uri().'/assets/js/wow.min.js',array('jquery'),'version',true);
    wp_enqueue_script('functions',get_template_directory_uri().'/assets/js/functions.js',array('jquery'),'version',true);
    wp_enqueue_script('viewport',get_template_directory_uri().'/assets/js/jquery.viewport.js',array('jquery'),'version',true);
    wp_enqueue_script('easypiechart',get_template_directory_uri().'/assets/js/jquery.easypiechart.min.js',array('jquery'),'version',true);
    wp_enqueue_script('scripts',get_template_directory_uri().'/assets/js/scripts.js',array('jquery'),'version',true);



}
add_action('wp_enqueue_scripts','include_css_js');

function load_js(){ ?>
    <script type="text/javascript">

        jQuery('#main-nav').onePageNav({
        currentClass: 'active',
        changeHash: false,
        scrollSpeed: 1200,
        scrollOffset: 80,
        filter: ':not(.sub-menu a, .not-home)'
    });

        jQuery('.carousel').carousel({
        interval: 8000
    })

    /*  - wow animation wow.js
     ---------------------------------------------------*/
    var wow = new WOW (
        {
            boxClass:     'wow',      // animated element css class (default is wow)
            animateClass: 'animated', // animation css class (default is animated)
            offset:       0,          // distance to the element when triggering the animation (default is 0)
            mobile:       false       // trigger animations on mobile devices (true is default)
        }
    );
    wow.init();

    /*  - wow animation wow.js End
     ---------------------------------------------------*/

</script>
<?php
}

add_action('wp_footer','load_js',100);